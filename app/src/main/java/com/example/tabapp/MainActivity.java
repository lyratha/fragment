package com.example.tabapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.widget.FrameLayout;
import android.widget.TableLayout;

import com.google.android.material.tabs.TabLayout;

public class MainActivity extends AppCompatActivity {
FrameLayout frameLayout;
TabLayout tabLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        frameLayout=findViewById(R.id.container);
        tabLayout=findViewById(R.id.apptab);
                FragmentManager fragmentManager=getSupportFragmentManager();
       FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
       Homefragment homefragment=new Homefragment();
       fragmentTransaction.add(R.id.container,homefragment,"HOME");
       fragmentTransaction.commit();

        tabLayout.addOnTabSelectedListener(new TabLayout.BaseOnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int position=tab.getPosition();
                switch (position){
                    case 0:
                         FragmentManager fragmentManager=getSupportFragmentManager();
                         FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
                         Homefragment homefragment=new Homefragment();
                         fragmentTransaction.replace(R.id.container,homefragment,"HOME");
                         fragmentTransaction.commit();
                        break;
                    case 1:
                        FragmentManager fragmentManager1=getSupportFragmentManager();
                        FragmentTransaction fragmentTransaction1=fragmentManager1.beginTransaction();
                        AboutFragment aboutFragment=new AboutFragment();
                        fragmentTransaction1.replace(R.id.container,aboutFragment,"ABOUT");
                        fragmentTransaction1.commit();
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


    }
}
