package com.example.tabapp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentStatePagerAdapter;

public class ViewPagerAdapter extends FragmentStatePagerAdapter {   // or FragmentPagerAdapter
    private int tabcount;

    public ViewPagerAdapter(FragmentManager fm,int tabcount) {
        super(fm);
        this.tabcount=2;
    }


    @Override
    public Fragment getItem(int position) {
        Fragment fragment=null;
        switch (position){
            case 0:
                 fragment=new Homefragment();
                break;
            case 1:
                fragment=new AboutFragment();
                break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return tabcount;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
       CharSequence title=null;
       switch (position){
           case 0:
               title="Home";
               break;
           case 1:
               title="About";
               break;

       }
        return title;
    }
}
